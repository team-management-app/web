
## Quick start

  ```bash
  # install dependencies
  npm install
  
  # build all vendor modules (only once)
  npm run dll
  
  # serve with hot reload at localhost:3333
  npm run dev
  
  # build for github pages
  npm run gh
  ```

  
